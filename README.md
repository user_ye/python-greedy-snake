## 仅供学习参考使用

## 参考代码：

- 广度 寻路 https://gitee.com/mjreams/AI_snake/blob/master/tanchishe.py#

## 安装说明：

- 安装 python
- `pip install pygame`

## 运行： cmd

```
py main.py
```

### 游戏说明：

- 按空格键开始游戏
- 方向键上下左右控制

### 游戏图片：

- ![](./game1.png)
- ![](./game2.png)

### 自动寻路没写完后面扩展
